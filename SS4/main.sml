val fm = [[1,~2],[1,3],[2,3]];

val fm = [[1,~2],[5, ~1, 7],[2, 3, ~7], [~3, 2], [3, ~1]];

datatype State = UNKNOWN | SATISFIED | UNSATISFIABLE;

fun stateOf formula =
    if null formula
    then SATISFIED
    else isUnsatisfiable formula
and isUnsatisfiable formula =
    case formula of
        [] => UNKNOWN
      | head::tail =>
            if null head
            then UNSATISFIABLE
            else isUnsatisfiable tail;

fun assumeTrue (formula, areTrue) =
    case areTrue of
        [] => formula
      | head::tail => assumeTrue (assumeThatLitIsTrue (formula, head), tail)
and assumeThatLitIsTrue (formula, isTrue) =
    List.foldr (
        fn (cl, all) =>
            if List.exists (fn lit => isTrue = lit) cl
            then all
            else (List.filter (fn lit => not (lit = ~isTrue)) cl)::all
    ) [] formula;

fun unitPropagation (formula, valuation) =
    let 
        val units = 
            List.map (fn el => hd el)
                (List.filter (fn cl => length(cl) = 1) formula);
        val newValuation = units @ valuation;
    in
        (assumeTrue (formula, units), newValuation)
    end;

fun onlyUniques formula =
    List.foldr (
        fn (lit1, all) => 
            if List.exists (fn lit2 => lit1 = lit2) all
            then all
            else lit1::all
    ) [] (List.foldr (fn (cl, all) => all @ cl) [] formula);

fun displayList lst = print ("[" ^ (String.concatWith "," (map (fn el => Int.toString el) lst)) ^ "]\n");

fun DPLL (formula, valuation) =
    let
        (* 1. Propagate units. *)
        val (formula, valuation) = unitPropagation (formula, valuation)
        (* 2. Check the state of the formula. *)
        val state = (stateOf formula)
        
        val _ = displayList valuation
    in
        if (state = SATISFIED) orelse (state = UNSATISFIABLE)
        then (state, valuation)
        else 
            (* 3. Branch. *)
            branchFor (formula, onlyUniques formula, valuation)
    end
and branchFor (formula, selected, valuation) =
    case selected of
        [] => (UNSATISFIABLE, valuation)
      | head::tail =>
            let
                val (state, nValuation) =
                    DPLL (
                        assumeTrue (formula, [head]),
                        head::valuation)
            in
                if (state = SATISFIED)
                then (state, nValuation)
                else branchFor (formula, tail, valuation)
            end;

(* Parser for files in DIMACS format. *)
structure Parser =
struct

fun ignoreComments stream =
    let
        TextIO.inputLine stream
    in

    end

fun stream2ints stream =
    let
        val n = TextIO.scanStream (Int.scan StringCvt.DEC) stream
    in
        if isSome n
        then (valOf n)::(stream2ints stream)
        else []
    end;

fun ints2Internal ints =
    aux_ints2Internal (ints, [])
and aux_ints2Internal (ints, acc) =
    case ints of
        [] => if null acc then [] else [acc]
      | head::tail =>
            if (head = 0)
            then acc::(aux_ints2Internal (tail, []))
            else aux_ints2Internal (tail, (head::acc));

end

(* main *)

exception InvalidArgument of string;

val args = CommandLine.arguments();

val check_arguments =
    if not ((length args) = 1)
    then raise InvalidArgument "The number of arguments is invalid."
    else true

val formula = Parser.ints2Internal (Parser.stream2ints (TextIO.openIn (hd args)));

(* val (status, validation) = DPLL (formula, []); *)

(* val check_validation = assumeTrue (formula, validation); *)

(* OS.Process.exit(OS.Process.success) *)
