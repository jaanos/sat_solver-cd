from enum import Enum

class Literal:
    class State(Enum):
        TRUE = 0
        FALSE = 1
        UNKNOWN = 3

    def __init__(self, name, negated = False, level = 0, implier = None):
        self.name = name
        self.negated = negated
        self.level = level
        self.implier = implier

    def __eq__(self, other):
        return \
            (self.name == other.name) and \
            (self.negated == other.negated)

    def __str__(self):
        if self.negated:
            prefix = "!"
        else:
            prefix = ""
        return prefix + self.name

    def __hash__(self):
        return hash((self.name, self.negated))

    def __neg__(self):
        return Literal(self.name, not self.negated)

    def evaluateWith(self, assumedTrue):
        """ Evaluate the literal with the following literals assumed true. """

        if self in assumedTrue:
            return T
        elif -self in assumedTrue:
            return F
        else:
            return U

Lit = Literal
IS_TRUE = Lit("T")
T = Literal.State.TRUE
F = Literal.State.FALSE
U = Literal.State.UNKNOWN

class Clause:
    class State(Enum):
        UNSATISFIABLE = 0
        SATISFIED = 1
        UNIT = 2
        UNKNOWN = 3

    def __init__(self, *literals):
        if len(literals) == 0:
            raise exception.InvalidArgument(
                "An empty Clause is not supported.")

        self._literals = list(literals)

        self._state = Clause.State.UNKNOWN
        self._updateState()
    
    def __str__(self):
        return "(" + " V ".join(str(lt) for lt in self._literals) + ")"

    def state(self):
        return self._state.name + ", " + str(self)

    def assumeTrue(self, literal):
        # Check if literal is in the clause.
        if literal in self._literals:
            self._literals = [IS_TRUE]
        else: 
            try:
                # Check if negated literal is in the clause.
                self._literals.remove(-literal)
            except ValueError:
                # The clause did not contain the negated literal.
                pass

        return self._updateState()

    def _updateState(self):
        if len(self._literals) == 0:
            self._state = Clause.State.UNSATISFIABLE
        elif len(self._literals) == 1:
            if self._literals[0] == IS_TRUE:
                self._state = Clause.State.SATISFIED
            else:
                self._state = Clause.State.UNIT
        else:
            self._state = Clause.State.UNKNOWN

        return self._state

class Formula:

    class State(Enum):
        UNSATISFIABLE = 0
        SATISFIED = 1
        UNKNOWN = 2

    def __init__(self, *clauses):
        self._clauses = clauses
        self._state = Formula.State.UNKNOWN
        self._assumedTrue = []

    def __str__(self):
        return " & ".join(str(cl) for cl in self._clauses)

    def state(self):
        return (
            "F is " + self._state.name + ".\n" +
            "\n".join(("> " + str(cl.state())) for cl in self._clauses)
        )

    def getState(self):
        return self._state

    def assumeTrue(self, literal):
        self._assumedTrue.append(literal)

        for cl in self._clauses:
            cl.assumeTrue(literal)

        return self._updateState()

    def getUnits(self):
        units = []
        for cl in self._clauses:
            if cl._state == Clause.State.UNIT:
                if not (cl._literals[0] in units):
                    units.append(cl._literals[0])
        return units

    def getUnassignedLiterals(self):
        unLiterals = []
        for cl in self._clauses:
            if (cl._state == Clause.State.UNKNOWN or
                cl._state == Clause.State.UNIT):
                    for lit in cl._literals:
                        if not (lit in unLiterals):
                            unLiterals.append(lit)

        return unLiterals

    def _updateState(self):
        sCnt = 0
        for cl in self._clauses:
            if cl._state == Clause.State.SATISFIED:
                sCnt += 1
            elif cl._state == Clause.State.UNSATISFIABLE:
                self._state = Formula.State.UNSATISFIABLE
                return self._state

        if sCnt == len(self._clauses):
            self._state = Formula.State.SATISFIED
            return self._state
        else:
            self._state = Formula.State.UNKNOWN
            return self._state

if __name__ == "__main__":
    fr = Formula(
            Clause(Lit("x", True), Lit("y"), Lit("z")),
            Clause(Lit("y"), Lit("z", True)),
            Clause(Lit("y")),
            Clause(Lit("y"))
        )

    print(fr)

    print(fr.state())
    
    frc = Formula(*fr._clauses)
    print(frc.state())
    print(id(fr))
    print(id(frc))

    #print([str(unit) for unit in fr.getUnits()])
    #fr.assumeTrue(Lit("y", True))
    #print(fr.state())

    #print([str(lit) for lit in fr.getUnassignedLiterals()]) 
