from collections import namedtuple
from expression import *

#
# Functions for parsing DIMACS file
#

class InvalidArgument(Exception):
    pass

def DIMACSClause2Expression(DIMACSClause):
    literals = []
    splitClause = [int(x) for x in DIMACSClause.split()]
    # Check that the DIMACS clause is not empty and
    # that the last number is always 0.
    if len(splitClause) == 0 or not splitClause[-1] == 0:
        raise InvalidArgument(
            "A DIMACS clause is empty or the last number is not 0. " +
            "clause = " + str(DIMACSClause))

    for num in splitClause[:-1]:
        if num > 0:
            literals.append(Lit("x" + str(num)))
        elif num < 0:
            literals.append(Lit("x" + str(num * -1), True))
        else:
            raise InvalidArgument(
                "A DIMACS clause contains 0 that is not the last number. " +
                "clause = " + str(DIMACSClause))
        
    return Clause(*literals) 

def fromFile(path):
    """Create a CNF formula from a DIMACS file.
    
    The function reads a file and returns a CNF formula. The content of a file
    is expected to be in a DIMACS format.

    Parameters
    ----------
    path : string
        a path to a file containing a CNF formula in DIMACS format

    Returns
    -------
    namedtuple("DIMACSFile",["path","nbVar","nbClauses","formula"])
        Returns a namedtuple representing the content of a DIMSE format.
    """
    
    # Open a file.
    fl = open(path, "r")
    lines = fl.readlines()

    i = 0
    # Ignore comments and empty lines. 
    for i in range(0, len(lines)):
        strLine = lines[i].strip()
        if len(strLine) == 0:
            # Ignore empty lines.
            continue
        elif strLine[0] == "c":
            # Ignore comments.
            continue
        else:
            break

    # Parse ID line.
    idLine = lines[i].split()
    if not (len(idLine) == 4 and idLine[0] == "p" and idLine[1] == "cnf"):
        raise InvalidArgument(
            "DIMACS ID line is not valid. " + 
            "ID line = " + str(idLine))

    nbVar = int(idLine[2])
    nbClauses = int(idLine[3])
   
    # Parse clauses.
    clauses = []
    for j in range(i + 1, len(lines)):
        strLine = lines[j].strip()
        if strLine[0] == "%":
            break
        clauses.append(DIMACSClause2Expression(lines[j]))
    
    DIMACSFile = namedtuple(
        "DIMACSFile",
        ["path",
         "nbVar",
         "nbClauses",
         "formula"]
    )

    return DIMACSFile(
        path=path,
        nbVar=nbVar,
        nbClauses=nbClauses,
        formula=Formula(*clauses))

def getValuationFromFile(path):
    fl = open(path, "r")
    lines = fl.readlines()

    if not (len(lines) == 1):
        raise InvalidArgument(
            "Multiple lines present. Only a single line expected."); 

    nums = [int(x) for x in lines[0].strip().split()]

    literals = []
    for num in nums:
        if num > 0:
            literals.append(Lit("x" + str(num)))
        elif num < 0:
            literals.append(Lit("x" + str(num * -1), True))
        else:
            raise InvalidArgument("The valuation contains 0 integer.");

    return literals
