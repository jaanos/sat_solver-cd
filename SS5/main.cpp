#include <iostream>
#include <fstream>
#include <iterator>
#include <sstream>
#include <vector>
#include <map>
#include <stack>
#include <algorithm>
#include <cmath>
#include <chrono>
#include <future>

#include <tclap/CmdLine.h>

std::random_device RD;
std::mt19937 GENERATOR(RD());
// std::uniform_int_distribution<std::mt19937::result_type> DIST(0,100);

enum class State
{
    UNKNOWN,
    UNIT,
    SATISFIED,
    UNSATISFIABLE,
    RESTART
};

std::map <State, std::string> MAP2STRING = {
    {State::UNKNOWN, "UNKNOWN"},
    {State::UNIT, "UNIT"},
    {State::SATISFIED, "SATISFIED"},
    {State::UNSATISFIABLE, "UNSATISFIABLE"},
    {State::RESTART, "RESTART"}
};

class Clause
{
private:
    struct Step
    {
        Step (
            const size_t dl,
            const int headIndex,
            const int tailIndex,
            const State state)
            : m_dl(dl),
              m_headIndex(headIndex),
              m_tailIndex(tailIndex),
              m_state(state)
        {}

        // decision level
        const size_t m_dl;
        
        // pointers
        const int m_headIndex;
        const int m_tailIndex;
        
        // state
        const State m_state;
    };
    
public:
    Clause (const std::vector<int> &lits)
        : m_lits(lits)
    {
        // Assign initial state.
        State state = State::UNKNOWN;
        switch (m_lits.size())
        {
            case 0:
                throw std::invalid_argument ("Error: The list of clauses is empty."); 
            case 1:
                state = State::UNIT;
                break;
            default:
                state = State::UNKNOWN;
                break;
        }

        m_steps.push(Step(0, 0, m_lits.size() - 1, state));
    }
    
    State getState() const
    {
        return m_steps.top().m_state;
    };
    
    int getHad() const
    {
        return m_lits.at(getHeadIndex());
    }
    
    int getTail() const
    {
        return m_lits.at(getTailIndex());
    }
    
    /*
    void assumeTrue(const size_t dl, const std::vector<int> val)
    {
        if ((getState() == State::SATISFIED) || (getState() == State::UNSATISFIABLE))
        {
            return;
        }
        
        if ((m_lits.at(getHeadIndex()) == val.back()) ||
                (m_lits.at(getTailIndex()) == val.back()))
        {
            m_steps.push(Step(dl, getHeadIndex(), getTailIndex(), State::SATISFIED));
            return;
        }
        
        // Check tail.
        if (-1 * m_lits.at(getHeadIndex()) == (val.back()))
        {
            m_steps.push(findNextHead(dl, val));
        }
        
        // Check head.
        else if (-1 * m_lits.at(getTailIndex()) == (val.back()))
        {
            m_steps.push(findNextTail(dl, val));
        }
    };
    */
    
    void assumeTrue(const size_t dl, const std::vector<int> val)
    {
        if ((getState() == State::SATISFIED) || (getState() == State::UNSATISFIABLE))
        {
            return;
        }
        
        auto newState = aux_assumeTrue(dl, m_steps.top().m_headIndex, m_steps.top().m_tailIndex, val);
        
        if (
            not (
                (m_steps.top().m_headIndex == newState.m_headIndex) &&
                (m_steps.top().m_tailIndex == newState.m_tailIndex) &&
                (m_steps.top().m_state == newState.m_state)
            )
        )
        {
            // If a step has changed
            
            m_steps.push(newState);
        }
        
        
        // If a step has not changed
    };
    
    
    Step aux_assumeTrue(
        const size_t dl,
        const int headIndex,
        const int tailIndex,
        const std::vector<int> val)
    {
        if (tailIndex < headIndex)
        {
            throw std::runtime_error("The tail is smaller than head.");
        }
        
        if (isTrue(val, m_lits.at(headIndex)) || isTrue(val, m_lits.at(tailIndex)))
        {
            return Step(dl, headIndex, tailIndex, State::SATISFIED);
        }
        
        if (headIndex == tailIndex)
        {
            if (isTrue(val, -1 * m_lits.at(headIndex)))
            {
                return Step(dl, headIndex, tailIndex, State::UNSATISFIABLE);
            }
            else
            {
                return Step(dl, headIndex, tailIndex, State::UNIT);
            }
        }
        
        // Check head.
        if (isTrue(val, -1 * m_lits.at(headIndex)))
        {
            return aux_assumeTrue(dl, headIndex + 1, tailIndex, val);
        }
        // Check tail.
        else if (isTrue(val, -1 * m_lits.at(tailIndex)))
        {
            return aux_assumeTrue(dl, headIndex, tailIndex - 1, val);
        }
        
        
        return Step(dl, headIndex, tailIndex, State::UNKNOWN);
    };
    
    void backtraceToLevel(const size_t dl)
    {
        while (m_steps.top().m_dl > dl)
        {
            m_steps.pop();
        }
    };

    friend std::ostream& operator<< (std::ostream& os, const Clause& cl);

    // Literals
    const std::vector<int> m_lits;
    
private:
    
    bool isTrue(const std::vector<int> val, int lit)
    {
        return (std::find(val.begin(), val.end(), lit) != val.end());
    }
    
    /*
    Step findNextHead(const size_t dl, const std::vector<int> val)
    {
        for (auto i = getHeadIndex(); i <= getTailIndex(); i++)
        {
            if (std::find(val.begin(), val.end(), m_lits.at(i)) != val.end())
            {
                // SATISFIED
                return Step(dl, i, getTailIndex(), State::SATISFIED);
            }

            if (std::find(val.begin(), val.end(), -1 * m_lits.at(i)) == val.end())
            {
                if (i == getTailIndex())
                {
                    // UNIT
                    return Step(dl, i, getTailIndex(), State::UNIT);
                }
                else
                {
                    // UNKNOWN
                    return Step(dl, i, getTailIndex(), State::UNKNOWN);
                }
            }
        }

        // UNSATISFIED
        return Step(dl, getTailIndex(), getTailIndex(), State::UNSATISFIABLE);
    }
    */
    
    /*
    Step findNextTail(const size_t dl, const std::vector<int> val)
    {
        std::cout << "TAIL" << std::endl;
        
        for (auto i = getTailIndex(); getHeadIndex() <= i; i--) // TODO
        {
            std::cout <<  getHeadIndex() << ", " << i << std::endl;
            
            if (std::find(val.begin(), val.end(), m_lits.at(i)) != val.end())
            {
                // SATISFIED
                return Step(dl, getHeadIndex(), i, State::SATISFIED);
            }

            if (std::find(val.begin(), val.end(), -1 * m_lits.at(i)) == val.end())
            {
                if (getHeadIndex() == i)
                {
                    // UNIT
                    return Step(dl, getHeadIndex(), i, State::UNIT);
                }
                else
                {
                    // UNKNOWN
                    return Step(dl, getHeadIndex(), i, State::UNKNOWN);
                }
            }
        }
        
        std::cout << "OUT" << std::endl;

        // UNSATISFIED
        return Step(dl, getHeadIndex(), getHeadIndex(), State::UNSATISFIABLE);
    }
    */
    
    int getHeadIndex () const
    {
        return m_steps.top().m_headIndex;
    };
    
    int getTailIndex () const
    {
        return m_steps.top().m_tailIndex;
    };

    // Steps
    std::stack<Step> m_steps;
};

std::ostream& operator<< (std::ostream& os, const Clause& cl)
{
    os << "[" <<
        MAP2STRING[cl.m_steps.top().m_state] << ", " <<
        cl.m_steps.top().m_dl << ", " <<
        cl.m_steps.top().m_headIndex << ", " <<
        cl.m_steps.top().m_tailIndex << "]";
        
    os << "[";
    for (auto& lit : cl.m_lits)
    {
        os << lit << ", ";
    }
    os << "]";
        
    return os;
};

class Formula
{
public:
    Formula(const std::vector<Clause>& formula)
        : m_formula(formula),
          m_backtraces(0)
    {
        m_valuations.push({});
    }
    
    void assumeTrue(const int lit);
    
    void propagateUnits();
    
    std::vector<int> getUnassignedLiterals() const;
    
    State getState() const;
    
    void pushDecisionLevel()
    {
        m_valuations.push(m_valuations.top());
    }
    
    size_t getDecisionLevel() const
    {
        return m_valuations.size();
    }
    
    void popDecisionLevel();
    
    void backtraceToLevel(const size_t dl);
    
    const std::vector<int>& getValuation() const
    {
        if (m_valuations.size() == 0)
        {
            throw std::runtime_error("The valuation stack is empty.");
        }
        
        return m_valuations.top();
    };
    
    size_t getNumberOfBacktraces() const
    {
        return m_backtraces;
    }

private:
    std::vector<int> getUnits() const;
    
    // formula
    std::vector<Clause> m_formula;
    
    // valuation
    std::stack<std::vector<int>> m_valuations;
    
    // number of backtraces
    size_t m_backtraces;
};

void Formula::assumeTrue(const int lit)
{
    m_valuations.top().push_back(lit);
    
    for (auto& clause : m_formula)
    {
        clause.assumeTrue(getDecisionLevel(), m_valuations.top());
    }
}

std::vector<int> Formula::getUnits() const
{
    std::vector<int> units;
    for (auto& clause : m_formula)
    {
        if (clause.getState() == State::UNIT)
        {
            units.push_back(clause.getHad());
        }
    }
    
    // Leave only unique units.
    std::sort(units.begin(), units.end());
    auto last = std::unique(units.begin(), units.end());
    units.erase(last, units.end()); 
    
    return units;
}

void Formula::propagateUnits()
{
    auto units = getUnits();
    while (not units.empty())
    {
        for (auto unit : getUnits())
        {
            assumeTrue(unit);
        }
        
        units = getUnits();
    }
}

State Formula::getState() const
{
    size_t satCnt = 0;
    for (auto& cl : m_formula)
    {
        if (cl.getState() == State::UNSATISFIABLE)
        {
            return State::UNSATISFIABLE;
        }
        else if (cl.getState() == State::SATISFIED)
        {
            satCnt++;
        }
    }
    
    if (satCnt == m_formula.size())
    {
        return State::SATISFIED;
    }
    
    return State::UNKNOWN;
}

std::vector<int> Formula::getUnassignedLiterals() const
{
    std::vector<int> lits;
    for (auto& clause : m_formula)
    {
        if (clause.getState() == State::UNIT)
        {
            lits.push_back(clause.getHad());
        }
        else if (clause.getState() == State::UNKNOWN)
        {
            lits.push_back(clause.getHad());
            lits.push_back(clause.getTail());
        }
    }
    
    // Leave only unique units.
    std::sort(lits.begin(), lits.end());
    auto last = std::unique(lits.begin(), lits.end());
    lits.erase(last, lits.end()); 
    
    return lits;
}

void Formula::popDecisionLevel()
{
    if (getDecisionLevel() == 0)
    {
        throw std::runtime_error("The decision level is 0.");
    }
    
    backtraceToLevel(getDecisionLevel() - 1);
}

void Formula::backtraceToLevel(const size_t dl)
{
    // Backtrace valuation.
    while(getDecisionLevel() > dl)
    {
        m_valuations.pop();
    }
    
    // Backtrace formula.
    for (auto& cl : m_formula)
    {
        cl.backtraceToLevel(dl);
    }
    
    m_backtraces++;
}

struct Result
{
    Result(const State state, const std::vector<int> valuation)
        : m_state(state), m_valuation(valuation)
        {};

    State m_state;
    std::vector<int> m_valuation;
};

// Recursion:

// 1. Propagate units.

// 2. Check state.

// 4. Get possible literals.
// for (unassigned literals)

// 5. Select an unassigned variable.
// 6. assumeTrue
// 7. ret = recursive
// 8. If ret is false, backtrace.

Result solveWithDPLL(
    Formula& formula,
    const bool random = true,
    const std::pair<bool, size_t> restart = {false, 0},
    const bool verbose = false)
{
    // 1. Propagate units.
    formula.propagateUnits();
    
    // 2. Check the state.
    auto state = formula.getState();
    
    if (state == State::SATISFIED)
    {
        return Result(State::SATISFIED, formula.getValuation());
    }
    else if (state == State::UNSATISFIABLE)
    {
        return Result(State::UNSATISFIABLE, formula.getValuation());
    }
    
    if (verbose)
    {
        std::cout << "> DL = " << formula.getDecisionLevel() << " ";
        std::cout << "val = [";
        auto val = formula.getValuation();
        std::copy(
            val.begin(), val.end(),
            std::ostream_iterator<int>(std::cout, ",")
        );
        std::cout << "]" << std::endl;
    }
    
    // 3. Get unassigned literals.
    auto unassigned = formula.getUnassignedLiterals();
    
    if (random)
    {
        std::shuffle(unassigned.begin(), unassigned.end(), GENERATOR);
    }
    
    for (auto un : unassigned)
    {
        // 4. Consider random restart.
        if ((restart.first == true) && (formula.getNumberOfBacktraces() > restart.second))
        {
            return Result{State::RESTART, formula.getValuation()};
        }
    
        // 5. Assume true.
        formula.pushDecisionLevel();
        formula.assumeTrue(un);

        // 6. Recursive.
        auto ret = solveWithDPLL(formula, random, restart, verbose);
        switch(ret.m_state)
        {
        case State::SATISFIED:
            return ret;
        case State::RESTART:
            return ret;
        case State::UNSATISFIABLE:
            formula.popDecisionLevel();
            break;
        default:
            throw std::runtime_error("The " + MAP2STRING[ret.m_state] + " was not expected.");
        }
    }

    return Result(State::UNSATISFIABLE, formula.getValuation());
};

Result solveWithDPLLwRst(const Formula& defFormula, bool verbose = false)
{
    size_t numOfRst = 0;
    size_t restBase = 200;
    
    auto formula = Formula(defFormula);
    auto status = solveWithDPLL(formula , true, {true, restBase}, verbose);
    while (status.m_state == State::RESTART)
    {
        numOfRst++;
        if (verbose)
        {
            std::cout << "RESTART " << numOfRst << std::endl;
        }
        
        formula = Formula(defFormula);
        status = solveWithDPLL(formula, true, {true, restBase + (numOfRst + std::pow(5,numOfRst))}, verbose);
    }
    
    return status;
}

std::vector<Clause> fromDimacsFile(const std::string& path)
{
    std::ifstream file(path);
    std::string line; 
    std::vector<Clause> clauses;
    while (std::getline(file, line))
    {
        if ((line.size() == 0) ||
            (line.at(0) == 'c') ||
            (line.at(0) == 'p') ||
            (line.at(0) == '%'))
        {
            continue;
        }
        
        std::stringstream ss(line);
        std::string item;
        std::vector<int> lits;
        while (std::getline(ss, item, ' '))
        {
            if ((item.size() == 0) || ((item.size() == 1) && (item.at(0) == ' ')))
            {
                // Ignore spaces.
                continue;
             }
            
            lits.push_back(std::stoi(item));
        }
        
        if (not (lits.back() == 0)) {
            throw std::runtime_error(
                "Problem with the DIMACS file. "
                "0 is not present at the end of a clause.");
        }
        
        lits.pop_back();
        
        if (lits.size() == 0)
        {
            // Ignore lines that only contain 0.
            continue;
        }
        
        clauses.push_back(Clause(lits));
    }
    
    return clauses;
}

void toFile(const std::string& path, const std::vector<int>& valuation)
{
    std::ofstream fl(path);
    
    for (auto lit : valuation)
    {
        fl << lit << " ";
    }
    
    fl.close();
}

int main(int argc, char **argv)
{
    TCLAP::CmdLine parser("SAT solver: Find a valuation that will satisfy the logical formula.");
    
    TCLAP::UnlabeledValueArg<std::string> from(
        "from",
        "a file, in a DIMACS format, containing a logical formula",
        true,
        "",
        "file path"
    );
    parser.add(from);
    
    TCLAP::UnlabeledValueArg<std::string> to(
        "to",
        "a file that will contain the result",
        true,
        "",
        "file path"
    );
    parser.add(to);
    
    TCLAP::ValueArg<uint16_t> numOfThreads(
        "t",
        "numberOfThreads",
        "a number of available threads",
        false,
        1,
        "UInt16"
    );
    parser.add(numOfThreads);
    
    
    TCLAP::SwitchArg info("i", "info", "Display result information.", false);
    parser.add(info);
    
    TCLAP::SwitchArg verbose("v", "verbose", "Display the runtime information.", false);
    parser.add(verbose);
    
    parser.parse(argc, argv);
    
    if (info.getValue() || verbose.getValue())
    {
        std::cout << "Arguments: " << std::endl;
        std::cout << "- from = " << from.getValue() << std::endl;
        std::cout << "- to = " << to.getValue() << std::endl;
        std::cout << "- number of threads = " << numOfThreads.getValue() << std::endl;
        std::cout << std::endl;
        std::cout << "Solve:" << std::endl;
    }
    
    if (numOfThreads.getValue() == 0)
    {
        throw std::invalid_argument("Invalid number of threads. The number of threads cannot be 0.");
    }
    
    auto formula = fromDimacsFile(from.getValue());
    
    if (formula.size() == 0)
    {
        std::cerr << "Error: The formula is empty." << std::endl;
        return 0;
    }
    
    auto cFormula = Formula(formula);
    
    auto start = std::chrono::steady_clock::now();
    
    //std::vector<std::packaged_task<Result(const Formula&, bool)>> tasks;
    std::vector<std::future<Result>> futures;
    for (auto i = 0; i < numOfThreads.getValue(); i++)
    {
        std::packaged_task<Result(const Formula&, bool)> task(solveWithDPLLwRst);
        futures.push_back(task.get_future());
        if (info.getValue() || verbose.getValue())
        {
            std::cout << "Starting thread " << i << "." << std::endl;
        }
        std::thread(std::move(task), cFormula, (verbose.getValue())).detach();
    }

    //auto ret = solveWithDPLLwRst(cFormula, verbose.getValue());
    
    auto ret = [&](){
        while (true)
        {
            for (auto& fut : futures)
            {
                if (fut.wait_for(std::chrono::milliseconds(10)) == std::future_status::ready)
                {
                    return fut.get();
                }
            }
        }
        throw std::runtime_error("The end.");
    }();
    
    auto end = std::chrono::steady_clock::now();
    
    auto diff = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    
    if (info.getValue() || verbose.getValue())
    {
        std::cout << std::endl;
        std::cout << "Result:" << std::endl;
        std::cout << "> used time = " << diff.count() << "ms" << std::endl;
        std::cout << "> state = [" << MAP2STRING[ret.m_state] << "]" << std::endl;
        
        std::cout << "valuation = [";
        std::copy(
                ret.m_valuation.begin(),
                ret.m_valuation.end(),
                std::ostream_iterator<int>(std::cout, ",")
        );
        std::cout << "]" << std::endl;
    }
    
    if (ret.m_state == State::SATISFIED)
    {
        toFile(to.getValue(), ret.m_valuation);
    }
    else
    {
        toFile(to.getValue(), {0});
    }
    
    return 0;
}
